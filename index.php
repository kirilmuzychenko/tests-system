<?php

require_once "view/header.php";

if (isset($_GET['do']) && $_GET['do'] == 'exit') {
    session_unset();
}

if (!empty($_POST)) {
    if (isset($_POST['login']) and isset($_POST['password'])) {

        $login = $_POST['login'];
        $login = trim($login);
        $psw = $_POST['password'];
        $psw = trim($psw);
        $psw = md5($psw);
        require_once "controller/userController.php";
        $checkLogin = new userController();
        $checkLogin->checkLoginPsw($login, $psw);
    }

    if (isset($_POST['newUser']) and isset($_POST['newPass']) and isset($_POST['rePass'])){

        $username = $_POST['newUser'];
        $password = $_POST['newPass'];
        $password = md5($password);
        $role = $_POST['role'];

        require_once "controller/userController.php";
        $addUser = new userController();
        $addUser->addUser($username, $password, $role);
    }

    if (isset($_POST['deleteUser'])){

        $login = $_POST['deleteUser'];
        $login = trim($login);

        require_once "controller/userController.php";
        $deleteUser = new userController();
        $deleteUser->deleteUser($login);
    }

    if ($_SESSION['role'] == 'user'){
        require_once "controller/testsController.php";
        $getTest = new testsController();
        $_SESSION['test'] = $getTest->get_test();
    }
}

?>
    <main>
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                <?php
                if ($_SESSION['role'] == 'admin') {
                    echo "Welcome, {$_SESSION['username']}!<br>";
                    ?>
                    <a class="btn btn-default a-profile" href='index.php?do=exit' role="button">Logout</a>

                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">Add new user</button>

                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <div class="modal-content">
                                <h2>Add new user</h2>
                                <form action="index.php" method="post">
                                    <div class="form-group">
                                        <label for="newUser">Login</label>
                                        <input name="newUser" type="text" class="form-control"  placeholder="Enter new user login">
                                    </div>
                                    <div class="form-group">
                                        <label for="newPass">Password</label>
                                        <input name="newPass" type="password" class="form-control"  placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <label for="rePass">Retype-Password</label>
                                        <input name="rePass" type="password" class="form-control"  placeholder="Retype-Password">
                                    </div>
                                    <div>
                                        <select class="form-control" name="role">
                                            <option value="admin">Admin</option>
                                            <option value="user">User</option>
                                        </select>
                                    </div>
                                    <button name="send" type="submit" class="btn btn-default">Add user</button>
                                </form>
                            </div>

                        </div>
                    </div>

                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal2">Delete user</button>

                    <div id="myModal2" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <div class="modal-content">
                                <h2>Delete user</h2>
                                <form action="index.php" method="post">
                                    <div class="form-group">
                                        <label for="deleteUser">Login</label>
                                        <input name="deleteUser" type="text" class="form-control"  placeholder="Enter deleted user">
                                    </div>
                                    <button name="send" type="submit" class="btn btn-default">Delete user</button>
                                </form>
                            </div>

                        </div>
                    </div>
                    
                    <a href="newTest.php" class="btn btn-default">Add new test</a>

                        <?php
                } elseif ($_SESSION['role'] == 'user') {
                    echo "Welcome, {$_SESSION['username']}!<br>";
                    ?>
                    <h1>Tests</h1>
                    <br>
                    <a class="btn btn-default a-profile" href='index.php?do=exit' role="button">Logout</a>
                    <br>
                    <?php
                    if (isset($_SESSION['test'])){
                        $test = $_SESSION['test'];
                        foreach ($test as $key=>$value){
                            foreach ($value as $value2){
                                echo "<p><a href='test.php'>Test name: $value2</a></p>";
                            }
                        }
                    }
                } else {?>
                    </div>
                </div>
                <form class="form-inline" method="post" action="index.php">
                    <div class="form-group">
                        <label class="sr-only" for="login">Email address</label>
                        <input type="text" class="form-control" name="login" placeholder="Login">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="password">Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-default">Sign in</button>
                </form>
                <?php }
                if (isset($_SESSION['addedUser'])){
                    echo $_SESSION['addedUser'];
                    unset($_SESSION['addedUser']);
                } elseif (isset($_SESSION['userExist'])){
                    echo $_SESSION['userExist'];
                    unset($_SESSION['userExist']);
                } elseif (isset($_SESSION['deletedUser'])){
                    echo $_SESSION['deletedUser'];
                    unset($_SESSION['deletedUser']);
                } elseif (isset($_SESSION['notDeletedUser'])) {
                    echo $_SESSION['notDeletedUser'];
                    unset($_SESSION['notDeletedUser']);
                } elseif (isset($_SESSION['loginError'])){
                    echo  $_SESSION['loginError'];
                    unset($_SESSION['loginError']);
                }
                ?>
            </div>
        </div>
    </main>

<?php
require_once "view/footer.php";