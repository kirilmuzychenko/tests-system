<?php
session_start();
require_once "model/dbManager.php";

class testsController{

    public function checkId(){
        $db = new dbManager();
        $checkIdResult = $db->checkIdTest();
        $test = mysqli_fetch_assoc($checkIdResult);
        $dbId = $test['id'];
        return $dbId;
    }

    function get_test(){
        $db = new dbManager();
        $get_tests = $db->getAllTest();
        $test = mysqli_fetch_all($get_tests);
        return $test;
    }

    public function addTest($id, $newTestName){
        $this->$newTestName = $newTestName;
        $this->id = $id;
        $db = new dbManager();
        $db->addNewTest($id, $newTestName);
    }

    public function addQuestion($id1, $newQuestion, $parentTest){
        $this->$newQuestion = $newQuestion;
        $this->parentTest = $parentTest;
        $this->id1 = $id1;
        $db = new dbManager();
        $db->addNewQuestion($id1, $newQuestion, $parentTest);
    }
}