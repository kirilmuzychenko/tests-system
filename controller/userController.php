<?php
session_start();
require_once "model/dbManager.php";

class userController{
    function checkLoginPsw($login, $psw){
        $this->login = $login;
        $this->psw = $psw;
        $db = new dbManager();
        $checkLoginPswResult = $db->checkUser($login);

        $test = mysqli_fetch_assoc($checkLoginPswResult);

        $dbLogin = $test['username'];
        $dbPsw = $test['password'];
        $dbRole = $test['role'];


        if ($psw == $dbPsw and $login == $dbLogin){
            $_SESSION['role'] = $dbRole;
            $_SESSION['username'] = $dbLogin;
            $_SESSION['password'] = $dbPsw;
        } else {
            $loginError =  "<p>Incorrect login or password</p>";
            $_SESSION['loginError'] = $loginError;
        }
    }

    public function addUser($username, $password, $role){
        $this->$username = $username;
        $this->$password = $password;
        $this->$role = $role;
        $db = new dbManager();
        $chackRes = $db->checkUser($username);
        $chackRes = mysqli_fetch_assoc($chackRes);
        $dbLogin = $chackRes['username'];
        if ($username == $dbLogin){
            $_SESSION['userExist'] = 'This user already exist';
        } else {
            $res = $db->addUser($username, $password, $role);
            if ($res == 1) {
                $_SESSION['addedUser'] = 'User successfully added!';
            } else {
                $_SESSION['addedUser'] = 'User not added!';
            }
        }
    }

    public function deleteUser($login){
        $this->$login = $login;
        $db = new dbManager();
        $checkRes = $db->checkUser($login);
        $checkRes = mysqli_fetch_assoc($checkRes);
        $dbLogin = $checkRes['username'];
        if ($login = $dbLogin){
            $db->deleteUser($login);
            $_SESSION['deletedUser'] = 'User deleted!';
        } else {
            $_SESSION['notDeletedUser'] = 'User is not found!';
        }
    }
}