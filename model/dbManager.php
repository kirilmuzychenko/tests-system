<?php
require_once "config.php";

class dbManager
{

    public $mysqli;

    function __construct()
    {
        $this->mysqli = new mysqli(HOST, DB, USERNAME, PASSWORD) or die("Connection error");
    }
    
    function checkUser($login){
        $checkUserResult = $this->mysqli->query("SELECT * FROM `users` WHERE `username` = '$login'");
        return $checkUserResult;
    }

    public function addUser($username, $password, $role) {
        $result = $this->mysqli->query("INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES (NULL, '$username', '$password', '$role')");
        return $result;
    }

    public function deleteUser($login) {
        $deleteResult = $this->mysqli->query("DELETE FROM `users` WHERE `username` = '$login'");
        return $deleteResult;
    }

    public function getAllTest(){
        $get_test = $this->mysqli->query("SELECT `test_name` FROM `test`");
        return $get_test;
    }

    public function addNewTest($id, $newTestName){
        $result = $this->mysqli->query("INSERT INTO `test` (`id`, `test_name`) VALUES ('$id', '$newTestName')");
        return $result;
    }

    public function addNewQuestion($id1, $newQuestion, $parentTest)
    {
        $result = $this->mysqli->query("INSERT INTO `questions` (`id`, `question`, `parent_test`) VALUES ('$id1', '$newQuestion', '$parentTest')");
        return $result;
    }

    public function checkIdTest(){
        $checkIdResult = $this->mysqli->query("SELECT `id` FROM `test`");
        return $checkIdResult;
    }

    function __destruct()
    {
        if ($this->mysqli) {
            $this->mysqli->close();
        }
    }
}